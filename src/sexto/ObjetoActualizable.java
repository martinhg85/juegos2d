/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sexto;

import java.awt.Graphics;

/**
 *
 * @author Martin
 */
public abstract class ObjetoActualizable extends ObjetoRenderizable {
    protected int ya;
    protected int xa;
    
    /**
     *
     */
    public abstract void update();
    @Override
    public abstract void render(Graphics g2d);

    /**
     * @return the ya
     */
    public int getYa() {
        return ya;
    }

    /**
     * @param ya the ya to set
     */
    public void setYa(int ya) {
        this.ya = ya;
    }

    /**
     * @return the xa
     */
    public int getXa() {
        return xa;
    }

    /**
     * @param xa the xa to set
     */
    public void setXa(int xa) {
        this.xa = xa;
    }
}
