package com.edu4java.minitennis7;

import java.applet.Applet;
import java.applet.AudioClip;

public class Sound {
	public static final AudioClip BALL = Applet.newAudioClip(Ball.class.getResource("ball.wav"));
	public static final AudioClip GAMEOVER = Applet.newAudioClip(Ball.class.getResource("gameover.wav"));
	public static final AudioClip BACK = Applet.newAudioClip(Ball.class.getResource("back.wav"));
}
