/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sexto;

import java.awt.Graphics;

/**
 *
 * @author Martin
 */
public abstract class ObjetoRenderizable {
    protected int x;
    protected int y;
    
    public abstract void render(Graphics g2d);

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(int y) {
        this.y = y;
    }
}
