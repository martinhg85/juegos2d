/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sexto;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author Martin
 */
public class Jugador extends Sprite implements KeyListener {

    private boolean izquierda;
    private boolean derecha;
    private boolean arriba;
    private boolean abajo;
    Jugador() {
    }

    public Jugador(int widthSprite, int heightSprite, String nombreImagen) {
        super(widthSprite, heightSprite, nombreImagen);
    }

    @Override
    public void update() {
        super.update(); //To change body of generated methods, choose Tools | Templates.
        xa = 0;
        ya=0;
        if (izquierda) {
            xa = -1;
        }
        if (derecha) {
            xa += 1;
        }
        if (arriba) {
            ya = -1;
        }
        if (abajo) {
            ya += 1;
        }

        x = x + xa;
        y = y + ya;

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            izquierda = true;
        }
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            derecha = true;
        }
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            arriba = true;
        }
        if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            abajo = true;
        }
        comprobarMovimiento();
        
      
    }

    @Override
    public void keyReleased(KeyEvent e) {
       
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            izquierda = false;
        }
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            derecha = false;
        }
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            arriba = false;
        }
        if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            abajo = false;
        }
        
        comprobarMovimiento();
        
        
        
   

    }

    private void comprobarMovimiento() {
        if ((izquierda && !derecha) || (!izquierda && derecha)) {
             if(izquierda)
                this.setFotogramaYActual(1);
             else
                this.setFotogramaYActual(0);
                 
            this.setCantidadFotogramas(3);
        }
        else if((arriba && !abajo) || ( !arriba && abajo)){
             if(arriba)
                this.setFotogramaYActual(2);
             else
                this.setFotogramaYActual(3);
                 
            this.setCantidadFotogramas(3);
        }
        else{
             this.setFotogramaXActual(0);
            this.setCantidadFotogramas(1);
        }
        
    }
}
