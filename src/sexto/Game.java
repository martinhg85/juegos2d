package sexto;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Game extends JPanel {

	Ball ball = new Ball(this);
	Racquet racquet = new Racquet(this);
        Jugador jugador = new Jugador( 50, 50,"zelda.png");
	public Game() {
		addKeyListener(jugador);
		setFocusable(true);
	}
	
	private void update() {
//		ball.move();
//		racquet.move();
                jugador.update();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		//ball.render(g2d);
		//racquet.paint(g2d);
                jugador.render(g);
	}
	
	public void gameOver() {
//		JOptionPane.showMessageDialog(this, "Game Over", "Game Over", JOptionPane.YES_NO_OPTION);
//		System.exit(ABORT);
	}
        public void render(){
            this.repaint();
        };
	public static void main(String[] args) throws InterruptedException {
		JFrame frame = new JFrame("Mini Tennis");
		Game game = new Game();
		frame.add(game);
		frame.setSize(300, 400);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		while (true) {
			game.update();
			game.render();
			Thread.sleep(10);
		}
	}
}
