/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sexto;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author Martin
 */
public class Sprite extends ObjetoActualizable {

    private int fotogramaXActual = 0;
    private int fotogramaYActual = 0;
    private int cantidadFotogramas;
    private int relacionFPS = 8;
    private int contadorRelacionFPS = 0;
    private int widthSprite;
    private int heightSprite;
    private int xSprite;
    private int ySprite;
    private ImageIcon imagenSprite;
    private JPanel game;
    private  String nombreImagen;
    private ImageIcon imagen;
// esto es dentro del metodo paint( Graphics g) del Canvas. 
    private Image img ;
    public Sprite(){}
    public Sprite(int widthSprite,int heightSprite,String nombreImagen){
       
        this.widthSprite=widthSprite;
        this.heightSprite=heightSprite;
        imagen = new ImageIcon(nombreImagen);
        img= imagen.getImage();
    }
    /**
     *
     */
    public void pasarFotogramaX() {
        // existe una relacion entre la cantidad de loops que da el juego y el cambio de fotograma
        // para controlar la velocidad de animacion se utiliza esta relación
        if (contadorRelacionFPS++ % relacionFPS == 0) {
            fotogramaXActual = ++fotogramaXActual % cantidadFotogramas;
        }
    }

    @Override
    public void update(){
        if(cantidadFotogramas>1)
            pasarFotogramaX();
        
    };

    @Override
    public void render(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        




        // dibujamos la imagen 

        g2d.drawImage(
                img,
                this.x,
                this.y,
                this.x + widthSprite,
                this.y + heightSprite,
                fotogramaXActual * widthSprite,
                fotogramaYActual * heightSprite,
                fotogramaXActual * widthSprite + widthSprite,
                fotogramaYActual * heightSprite + heightSprite,
                game);

    }

    ;

    /**
     * @return the fotogramaXActual
     */
    public int getFotogramaXActual() {
        return fotogramaXActual;
    }

    /**
     * @param fotogramaXActual the fotogramaXActual to set
     */
    public void setFotogramaXActual(int fotogramaXActual) {
        this.fotogramaXActual = fotogramaXActual;
    }

    /**
     * @return the fotogramaYActual
     */
    public int getFotogramaYActual() {
        return fotogramaYActual;
    }

    /**
     * @param fotogramaYActual the fotogramaYActual to set
     */
    public void setFotogramaYActual(int fotogramaYActual) {
        this.fotogramaYActual = fotogramaYActual;
    }

    /**
     * @return the cantidadFotogramas
     */
    public int getCantidadFotogramas() {
        return cantidadFotogramas;
    }

    /**
     * @param cantidadFotogramas the cantidadFotogramas to set
     */
    public void setCantidadFotogramas(int cantidadFotogramas) {
        this.cantidadFotogramas = cantidadFotogramas;
    }

    /**
     * @return the widthSprite
     */
    public int getWidthSprite() {
        return widthSprite;
    }

    /**
     * @param widthSprite the widthSprite to set
     */
    public void setWidthSprite(int widthSprite) {
        this.widthSprite = widthSprite;
    }

    /**
     * @return the heightSprite
     */
    public int getHeightSprite() {
        return heightSprite;
    }

    /**
     * @param heightSprite the heightSprite to set
     */
    public void setHeightSprite(int heightSprite) {
        this.heightSprite = heightSprite;
    }

    /**
     * @return the xSprite
     */
    public int getxSprite() {
        return xSprite;
    }

    /**
     * @param xSprite the xSprite to set
     */
    public void setxSprite(int xSprite) {
        this.xSprite = xSprite;
    }

    /**
     * @return the ySprite
     */
    public int getySprite() {
        return ySprite;
    }

    /**
     * @param ySprite the ySprite to set
     */
    public void setySprite(int ySprite) {
        this.ySprite = ySprite;
    }

    /**
     * @return the imagenSprite
     */
    public ImageIcon getImagenSprite() {
        return imagenSprite;
    }

    /**
     * @param imagenSprite the imagenSprite to set
     */
    public void setImagenSprite(ImageIcon imagenSprite) {
        this.imagenSprite = imagenSprite;
    }
}
