package com.edu4java.minitennis8;

import java.applet.Applet;
import java.applet.AudioClip;

public class Sound {
	public static final AudioClip BALL = Applet.newAudioClip(Ball.class.getResource("ball.wav"));
	public static final AudioClip GAMEOVER = Applet.newAudioClip(Ball.class.getResource("gameover.wav"));
	public static final AudioClip BACK = Applet.newAudioClip(Ball.class.getResource("back.wav"));

	public static void main(String[] args) throws InterruptedException {
		BACK.loop();
		for (int i = 0; i < 10; i++) {
			BALL.play();
			Thread.sleep(1000);
		}
		BACK.stop();
		GAMEOVER.play();
		Thread.sleep(1000);
	}
}
