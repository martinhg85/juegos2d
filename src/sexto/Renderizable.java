/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sexto;

import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author Martin
 */
interface Renderizable {
    public void render(Graphics2D g);
}
